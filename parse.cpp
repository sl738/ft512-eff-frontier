#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "asset.hpp"
using namespace std;

void parse_asset(string str, vector<asset_tag>& assetList)
{
    ifstream inFile(str, ios::in);
    if (!inFile)
    {
        perror("Open file error!\n");
        exit(1);
    }
    string line;
    string field;

    /* ----------------- Get the asset Info ----------------- */
    while (getline(inFile,line))
    {
        istringstream sin(line);
        getline(sin, field, ',');
        asset_tag asset;
        asset.name_t = field;
        getline(sin, field, ',');
        asset.avg_return = stod(field);
        getline(sin, field, ',');
        asset.std_deviation = stod(field);
        assetList.push_back(asset);
    }
    /* ------------------------------------------------------ */
}

void parse_cormtx(string str, vector <vector<double> >& corMtx)
{
    ifstream inFile(str, ios::in);
    if (!inFile)
    {
        perror("Open file error!\n");
        exit(1);
    }
    string line;
    string field;

    /* ------------- Get the correlation matrix------------- */
    while (getline(inFile,line))
    {
        vector<double> temp;
        istringstream sin(line);
        while(getline(sin, field, ','))
        {
            temp.push_back(stod(field));
        }
        corMtx.push_back(temp);
    }
    /* ----------------------------------------------------- */
}

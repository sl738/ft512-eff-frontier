#include <iomanip>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include </usr/local/include/eigen3/Eigen/Dense>
#include <unistd.h>
#include "parse.cpp"
using namespace std;
using namespace Eigen;
bool check_path(string path);
void remove_row(MatrixXd &x, int row);
void remove_col(MatrixXd &x, int col);

int main(int argc, char *argv[])
{
    /* ------------------------- Verify the input info ------------------------- */
    if(!(argc == 3 || argc == 4))
    {
        perror("Wrong number of parameters!\n");
        exit(1);
    }
    bool flagR = false;
    int opto;
    const char *optstr = "r";
    while ((opto = getopt(argc, argv, optstr)) != -1)
    {
        printf("optind: %d\n", optind);
        switch (opto)
        {
            case 'r':
                flagR = true;
                break;
            case '?':
                perror("Unknown operator!\n");
                exit(1);
        }
    }
    string str1(argv[optind]);
    string str2(argv[optind+1]);

     if(!check_path(str1)  || !check_path(str2))
    {
        perror("File path error!");
        exit(1);
    }
    /* ----------------------------------------------------------------------- */

    /* -------------- Get the asset and correlation matrix info -------------- */
    vector <asset_tag> assetList;
    parse_asset(str1, assetList);
    vector <vector <double> > corMtx;
    parse_cormtx(str2, corMtx);
    /* ----------------------------------------------------------------------- */

    /* ------------------- Calculate the covariance matrix ------------------- */
    vector<vector<double> > covMtx(assetList.size(), vector<double>(assetList.size(), 0));
    for(size_t i=0; i<assetList.size(); i++)
    {
        for(size_t j=i; j<assetList.size(); j++)
        {
            covMtx[i][j] = corMtx[i][j] * assetList[i].std_deviation * assetList[j].std_deviation\
;
        }
        for(size_t j=0; j<i; j++)
        {
            covMtx[i][j] = covMtx[j][i];
        }
    }
    /* ----------------------------------------------------------------------- */

     /* ------------------ Construct the covariance matrix -------------------- */
    MatrixXd T(assetList.size()+2, assetList.size()+2);
    for(size_t i=0; i<assetList.size(); i++)
    {
        for(size_t j=0; j<assetList.size(); j++)
        {
            T(i,j) = covMtx[i][j];
        }
    }
    for(size_t i=0; i<assetList.size(); i++)
    {
        T(assetList.size(), i) = 1;
	T(assetList.size()+1, i) = assetList[i].avg_return;
        T(i, assetList.size()) = 1;
        T(i, assetList.size()+1) = assetList[i].avg_return;
    }
    T(assetList.size(), assetList.size()) = 0;
    T(assetList.size(), assetList.size()+1) = 0;
    T(assetList.size()+1, assetList.size()) = 0;
    T(assetList.size()+1, assetList.size()+1) = 0;
    /* ----------------------------------------------------------------------- */

    /* ------------------------ initialize vector B -------------------------- */
    VectorXd B(assetList.size()+2);
    for (size_t i=0; i<assetList.size(); i++)
    {
        B(i) = 0;
    }
    B(assetList.size()) = 1;
    /* ----------------------------------------------------------------------- */
    cout << "ROR,volatility" << endl;
    /* -------------------------- Calculate the X ---------------------------- */
    for(int portfolio_n=1; portfolio_n<27; portfolio_n++)
    {
        B(assetList.size()+1) = 0.01 * portfolio_n;
        VectorXd xLamda(assetList.size()+2);
        xLamda = T.colPivHouseholderQr().solve(B);
        VectorXd X(assetList.size());
        MatrixXd theta(assetList.size(), assetList.size());
        X = xLamda.head(assetList.size());
        theta = T.topLeftCorner(assetList.size(), assetList.size());

        /* -------------------- Excute operator "-r" --------------------- */
        if(flagR)
        {
            int cnt = 0;
            bool flag[50] = {false};

            for(int i=0; i<xLamda.size()-2; i++)
            {
                if(xLamda(i) < 0)
                {
                    cnt ++;
                    flag[i] = true;
                }
            }
            // cout << "-------" << endl;                                                         
            // cout << T << endl;                                                                 
            // cout << "-------" << endl;                                                         
            // cout << xLamda << endl;   

            MatrixXd T1(T);
            for(int i=assetList.size()-1; i>=0; i--)
            {
                if(flag[i])
                {
                    remove_row(T1, i);
                    remove_col(T1, i);
                    // cout << "-------" << endl;                                                 
                    // cout << T1 << endl;                                                        
		}
            }
            while(cnt != 0)
            {
                xLamda = T1.colPivHouseholderQr().solve(B.tail(assetList.size()+2-cnt));
                // cout << "-------" << endl;                                                     
                // cout << xLamda << endl;                                                        
                size_t i, j = 0;
                for(i=0; i<assetList.size(); i++)
                {
                    if(flag[i])
                    {
                        X(i) = 0;
                    }
                    else
                    {
                        X(i) = xLamda(j++);
                    }
                }
                // cout << "-------" << endl;                                                     
                // cout << X << endl;    
                cnt = 0;
                bool tag = false;
                for(size_t i=0; i<assetList.size(); i++)
                {
                    if(X(i) < 0)
                    {
                        tag = true;
                        flag[i] = true;
                    }
                }
                if(tag)
                {
                    T1 = T;
                    for(int i=assetList.size()-1; i>=0; i--)
                    {
                        if(flag[i])
                        {
                            cnt ++;
                            remove_row(T1, i);
                            remove_col(T1, i);
                            // cout << "-------" << endl;                                         
                            // cout << T1 << endl;                                                
                        }
                    }
                }
            }
        }
        /* ------------------------------------------------------------- */    

        double var = 0;
        for(size_t i=0; i<assetList.size(); i++)
        {
            for(size_t j=0; j<assetList.size(); j++)
            {
                var += X(i) * X(j) * theta(i, j);
            }
        }
        double vol = sqrt(var);
        cout << fixed << setprecision(1) << portfolio_n*1.0 << "%,";
        cout << fixed << setprecision(2) << vol*100 << "%" << endl;
    }
    /* ----------------------------------------------------------------------- */
    return 0;
}

bool check_path(string path)
{
    if(path.length() < 4)
        return false;
    if(path.substr(path.length()-4, path.length()-4) == ".csv")
        return true;
    return false;
}

void remove_row(MatrixXd &x, int row)
{
    int rows = x.rows() - 1;
    int cols = x.cols();
    if(row < rows)
    {
        x.block(row, 0, rows-row, cols) = x.block(row+1, 0, rows-row, cols);
    }
    x.conservativeResize(rows, cols);
}
        
void remove_col(MatrixXd &x, int col)
{
    int rows = x.rows();
    int cols = x.cols() - 1;
    if(col < cols)
    {
        x.block(0, col, rows, cols-col) = x.block(0, col+1, rows, cols-col);
    }
    x.conservativeResize(rows, cols);
}

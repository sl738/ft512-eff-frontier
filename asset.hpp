File Edit Options Buffers Tools C++ Help                                        
#ifndef __ASSET_HPP__
#define __ASSET_HPP__

#include <string>

struct asset{
    std::string name_t;
    double avg_return;
    double std_deviation;
};
typedef struct asset asset_tag;

#endif
